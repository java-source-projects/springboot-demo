# netflix 1

This epic aims to learn about how to create endpoints (get, delete, post and put) and connect to an external API

# Index
- [Entities](#entities)
- [Endpoints](#endpoints)
    - [Category](#category)
    - [Tv Show](#tv-show)
    - [Season](#season)
    - [Chapter](#chapter)
    - [Actor](#actor)

## Entities

- As a developer, I want to create all the entites for the project

(Creation of this entities):

![Entities](/epics/media/epics/entities.png)

## Endpoints 

*The requested parameter for finding, deleting and adding will always be the entity's id

### Category

- As a user, I want to view all categories

    The result JSON value must include id and name


- As a user, I want to find an specific category

    The result JSON value must include id and name


- As a user, I want to delete an specific category

    This won't return a JSON

- As a user, I want to create a new category

    The result JSON value must include id and name


- As a user, I want to update data from existing category

    The result JSON value must include id and name



### Tv Show

- As a user, I want to view all tv shows

    The result JSON value must include id, name, short description, long description, year, recomended age and advertising

- As a user, I want to find an specific tv show

    The result JSON value must include id, name, short description, long description, year, recomended age and advertising

- As a user, I want to delete an specific tv show

    This won't return a JSON

- As a user, I want to create a new tv show

    The result JSON value must include id, name, short description, long description, year, recomended age and advertising

- As a user, I want to update data from an existing tv show

    The result JSON value must include id, name, short description, long description, year, recomended age and advertising

- As a user, I want to add an existing season to an existing tv show

    The result JSON value must include id, number and name of the season and the tv show

- As a user, I want to delete an existing season from an existing tv show

    This won't return a JSON

- As a user, I want to add an existing category to an existing tv show

    The result JSON value must include id, name of the category and the tv show

- As a user, I want to delete an existing category from an existing tv show

    This won't return a JSON

### Season

- As a user, I want to view all seasons

    The result JSON value must include id, number and name

- As a user, I want to find an specific season

    The result JSON value must include id, number and name

- As a user, I want to delete an specific season

    The result JSON value must include id, number and name

- As a user, I want to create a new season

    This won't return a JSON

- As a user, I want to update the data from an existing season

    The result JSON value must include id, number and name

- As a user, I want to add an existing chapter to an existing season

    The result JSON value must include id, number, name and chapters

- As a user, I want to delete an existing chapter from and existing season

    This won't return a JSON


### Chapter

- As a user, I want to view all chapters

    The result JSON value must include id, number, name and duration

- As a user, I want to find an specific chapter

    The result JSON value must include id, number, name and duration

- As a user, I want to delte an specific chapter

    This won't return a JSON

- As a user, I want to create a new chapter

    The result JSON value must include id, number, name and duration

- As a user, I want to update data from an existing chapter

    The result JSON value must include id, number, name and duration

- As a user, I want to add an actor to a chapter

    The result JSON value must include id, number, name, duration and actors

- As a user, I want to delete an actor from a chapter

    This won't return a JSON


### Actor

- As a user, I want to view all actors

    The result JSON value must include id, name and description

- As a user, I want to find an specific actor

    The result JSON value must include id, name and description

- As a user, I want to delte an specific actor

    This won't return a JSON

- As a user, I want to create a new actor

    The result JSON value must include id, name and description

- As a user, I want to update data from an existing actor

    The result JSON value must include id, name and description

- As a user, I want to view all tv shows and chapters a specific actor is in

    The result JSON value must include id, name, description, tv shows and chapters
