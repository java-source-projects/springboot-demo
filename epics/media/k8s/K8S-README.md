# Commands for run API with K8S in local

## Tools that you will need

- [ ] [Docker](https://www.docker.com/products/docker-desktop/): container manager
- [ ] [Docker Compose](https://docs.docker.com/compose/install/): you can use Docker Compose as command or plugin for Docker
- [ ] [Kubectl](https://kubernetes.io/docs/tasks/tools/): base command for Kubernetes
- [ ] [Lens](https://app.k8slens.dev/subscribe): this is an IDE for Kubernetes(K8S)


## Commands
With the following commands you can up the API using K8S in local environment
```
docker compose up -d
docker rm springboot-demo-app
docker tag springboot-demo-app basshamut/springboot-demo-app:0.0.1-SNAPSHOT
docker image rm springboot-demo-app
docker push basshamut/springboot-demo-app:0.0.1-SNAPSHOT
kubectl apply -f deployment-loadbalancer-services.yml
```
