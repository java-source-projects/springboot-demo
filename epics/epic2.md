# netflix 2

The aim of this epic is to be able to sort, paginate and filter data from our database.

## Sorting

- As a user, I want to sort an album by its year release, from oldest to newest
- As a user, I want to sort alphabetically an actor by its year release
- As a user, I want to use reverse dynamic sorting by name on a TV show

## Pagination 

- As a user, I want to get paginated all actors
- As a user, I want to get paginated all tv shows
- As a user, I want to get paginated all categories
- As a user, I want to get paginated all seasons
- As a user, I want to get paginated all chapters


## Filtering

- As a user, I want to filter the actors names that begins with 'A'
- As a user, I want to filter all the action tv shows
- As a user, I want to filter all chapters from season 1 whose name ends with "s"
- As a user, I want to filter a tv show its recomended age is over 12 and from the year 2017
