package org.demo.springboot.bootcamp.netflix.service;

import org.demo.springboot.bootcamp.netflix.persistance.entity.Category;
import org.demo.springboot.bootcamp.netflix.persistance.repository.CategoryRepository;
import org.demo.springboot.bootcamp.netflix.service.mapper.CategoryMapper;
import org.demo.springboot.bootcamp.netflix.service.mapper.dto.CategoryDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("categoryService")
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDTO> findAll() {
        List<Category> categories = categoryRepository.findAll();
        return categories.stream().map(category -> CategoryMapper.MAPPER.mapToDto(category)).collect(Collectors.toList());
    }

}
