package org.demo.springboot.bootcamp.netflix.service;

import org.demo.springboot.bootcamp.netflix.service.mapper.dto.CategoryDTO;

import java.util.List;

public interface CategoryService {
    List<CategoryDTO> findAll();
}
