package org.demo.springboot.bootcamp.netflix.controller;

import org.demo.springboot.bootcamp.netflix.service.CategoryService;
import org.demo.springboot.bootcamp.netflix.service.CategoryServiceImpl;
import org.demo.springboot.bootcamp.netflix.service.mapper.dto.CategoryDTO;
import org.demo.springboot.bootcamp.netflix.util.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("categoryController")
@RequestMapping(value = Constants.BASE_API_PATH + Constants.API_VERSION_PATH)
@Validated
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(Constants.CATEGORIES_PATH)
    @ResponseBody
    public ResponseEntity<List<CategoryDTO>> findAll() {
        return ResponseEntity.ok().body(categoryService.findAll());
    }

}
