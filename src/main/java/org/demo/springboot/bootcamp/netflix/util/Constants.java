package org.demo.springboot.bootcamp.netflix.util;

public class Constants {
    public static final String BASE_API_PATH = "/netflix-api";
    public static final String API_VERSION_PATH = "/v1";
    public static final String CATEGORIES_PATH = "/categories";

    public static final String QUERY_PARAM_NAME = "[name]";
    public static final Integer NOT_FOUND_CODE = 404;
    public static final String NOT_FOUND_MESSAGE = "Not Found";

    public static final Integer INTERNAL_SERVER_ERROR_CODE = 500;
    public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error";
}
