package org.demo.springboot.bootcamp.netflix.persistance.repository;

import org.demo.springboot.bootcamp.netflix.persistance.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
