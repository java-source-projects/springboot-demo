package org.demo.springboot.bootcamp.netflix.service.mapper;

import org.demo.springboot.bootcamp.netflix.persistance.entity.Category;
import org.demo.springboot.bootcamp.netflix.service.mapper.dto.CategoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", imports = {Category.class, CategoryDTO.class})
public interface CategoryMapper {

    CategoryMapper MAPPER = Mappers.getMapper(CategoryMapper.class);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    CategoryDTO mapToDto(Category entity);
}
