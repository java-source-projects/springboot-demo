package org.demo.springboot.bootcamp.netflix.controller.handler;

import org.demo.springboot.bootcamp.netflix.service.mapper.dto.ErrorDTO;
import org.demo.springboot.bootcamp.netflix.exceptions.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class GeneralControllerExceptionHandler {

	@ExceptionHandler(value = {ServiceException.class})
    @ResponseBody
    public ErrorDTO handleServiceException(ServiceException e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setErrorMessage(e.getMessage());
        return errorDTO;
    }
}
