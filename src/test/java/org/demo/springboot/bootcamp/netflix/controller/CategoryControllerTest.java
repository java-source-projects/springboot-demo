package org.demo.springboot.bootcamp.netflix.controller;

import org.demo.springboot.bootcamp.netflix.service.CategoryServiceImpl;
import org.demo.springboot.bootcamp.netflix.service.mapper.dto.CategoryDTO;
import org.demo.springboot.bootcamp.netflix.util.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryServiceImpl categoryServiceImpl;

    @Test
    public void shouldHTTPCodeOk200() throws Exception {
        final List<CategoryDTO> list = new ArrayList<>();
        final String path = Constants.BASE_API_PATH + Constants.API_VERSION_PATH + Constants.CATEGORIES_PATH;

        Mockito.when(categoryServiceImpl.findAll()).thenReturn(list);

        mockMvc.perform(get(path).header("Origin", "*"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }
}
